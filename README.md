# Helmholtz Imaging CONNECT, hosted at DESY

- <https://connect.helmholtz-imaging.de/>

## Data reported

- 'data.csv': number of registered users
   - Users: Number of registered 'experts'
   - Usage: Number of entries (cumulated number from all types of entries, e.g. application, facilities, etc)
   
## Source

- Taken from [Helmholtz Imaging Report](https://syncandshare.desy.de/index.php/s/xsRsc7xTFeZewxM), p 48.
